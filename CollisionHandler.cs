using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    private void OnCollisionEnter(Collision other) {
        switch (other.gameObject.tag) {
            case "Friendly":
                Debug.Log("Hit start pad.");
                break;
            case "Finish":
                Debug.Log("Hit end pad.");
                break;
            default:
                Debug.Log("Hit an object.");
                break;
        }
    }
}
