using System.Collections;
using System.Collections.Generic;
// using "import"
using UnityEngine;

// : "extends MonoBehaviour"
public class Movement : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] float yThrust = 200f;
    [SerializeField] float zAngle = 1f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
        
    }

// GetKey -- holding down 
// GetKeyDown -- pressed
    void ProcessInput() {
        if (Input.GetKey(KeyCode.Space)) {
            // adds a force relative to the objects coordinate system
            // Vector3(0,1,0)
           rb.AddRelativeForce(Vector3.up * yThrust * Time.deltaTime);
        }
        // Vector3(0,0,1)
        if (Input.GetKey(KeyCode.A)) {
            Rotate(1);
        }
        else if (Input.GetKey(KeyCode.D)) {
            Rotate(-1);
        }
    }

    void Rotate(int pos) {
        // freezing rotation so that the physics system (when colliding with objects) doesn't intefere with controls
        rb.freezeRotation = true;
        transform.Rotate(Vector3.forward * pos * zAngle * Time.deltaTime);
        rb.freezeRotation = false;
    }
}
